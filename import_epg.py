#!/usr/bin/python2.7
# -*- coding: utf8 -*-

import time
import sqlobject
from sqlobject import *
import MySQLdb.converters
import sys, os
import re
from sqlobject.col import StringCol
import string

def _mysql_timestamp_converter(raw):
         """Convert a MySQL TIMESTAMP to a floating point number representing
         the seconds since the Un*x Epoch. It uses custom code the input seems
         to be the new (MySQL 4.1+) timestamp format, otherwise code from the
         MySQLdb module is used."""
         if raw[4] == '-':
             return time.mktime(time.strptime(raw, '%Y-%m-%d %H:%M:%S'))
         else:
             return MySQLdb.converters.mysql_timestamp_converter(raw)
 
conversions = MySQLdb.converters.conversions.copy()
conversions[MySQLdb.constants.FIELD_TYPE.TIMESTAMP] = _mysql_timestamp_converter
 
MySQLConnection = sqlobject.mysql.builder()
connection = MySQLConnection(user='streamer', password='streamer', db='streamer', conv=conversions, use_unicode = True, charset = "utf8")
sqlhub.processConnection = connection
 
# data from /var/cache/vdr/epg.data
 
class Channel(SQLObject):
    vdrId = StringCol()
    name = StringCol()
     
class Event(SQLObject):
    vdrId = IntCol()
    startTime = BigIntCol()
    endTime = BigIntCol()
    duration = IntCol()
    title = StringCol()
    shortText = StringCol()
    description = StringCol()
    channel = ForeignKey('Channel')
     
def parseChannel(line):
    #     C     <channel id> <channel name>
    global channelFile
    global channelDB
    channelFile = ChannelFile()
    
    vdrIdEnd = line.find(' ')
    vdrId = str(line[:vdrIdEnd])
    name = line[vdrIdEnd+1:-1]
    print(vdrId, name)
    channelFile.vdrChannelId = vdrId
    channelFile.vdrName = name
    #   insert to DB
    
    # look for channel
    currentChannel = Channel.select(Channel.q.vdrId==channelFile.vdrChannelId)
    res = list(currentChannel)
    if(len(res)>0):
        channelDB = res[0]
    else:
        channelDB = Channel(vdrId=channelFile.vdrChannelId, name=channelFile.vdrName )
    
def parseChannelEnd():
    print("parseChannelEnd")
    pass
    

def parseEvent(line):
#     E     <event id> <start time> <duration> <table id> <version>
    global eventFile
    eventFile = EventFile()
    
    end = line.find(' ')
    vdrEventId = int(line[:end])
    
    newEnd = line.find(' ', end + 1)
    startTime = int(line[end+1: newEnd])
    end = newEnd
    
    newEnd = line.find(' ', end + 1)
    duration = int(line[end+1: newEnd])
    end = newEnd
    
#     print (vdrEventId, startTime, duration)
    
    eventFile.vdrId = vdrEventId
    eventFile.startTime = startTime
    eventFile.duration = duration
    
def parseEventEnd():
    global eventFile

# check if item exist
    end = eventFile.duration*1000 +  eventFile.startTime * 1000
#     end = (eventFile.duration*1000 +  eventFile.startTime) * 1000
    res = Event.select(AND(Event.q.channel==channelDB, Event.q.startTime>=eventFile.startTime*1000, Event.q.startTime < end ))
    if len(list(res)) == 0:
        print("parseEventEnd", eventFile.vdrId, eventFile.startTime, time.gmtime(eventFile.startTime))
        eventDB = Event(vdrId= eventFile.vdrId, startTime=eventFile.startTime*1000, endTime=end, duration=eventFile.duration, title=eventFile.title, shortText=eventFile.shortText,
                    description = eventFile.description, channel = channelDB )
    else:
#         print('item exist')
        pass

def parseTitle(line):
    global eventFile
    #print (line[:-1]).decode('utf-8')
    eventFile.title = str(line[:-1])    

def parseShortText(line):
    global eventFile
    #print (line[:-1]).decode('utf-8')
    eventFile.shortText = str(line[:-1])    
    
def parseDescription(line):
    global eventFile
    #print (line[:-1]).decode('utf-8')
    eventFile.description = str(line[:-1])

class EventFile():
    vdrId = 0
    startTime = 0
    duration = 0
    title = ''
    shortText = ''
    description = ''

class ChannelFile():
    vdrChannelId = ''
    vdrName = ''
    

if len(sys.argv)!=2:
    exit()

epgdatafile = sys.argv[1]

eventDB = 0
channelDB = 0

# Event.dropTable()
# Channel.dropTable()


try:
    Channel.createTable()
except:
    pass

try:
    Event.createTable()
except:
    pass    

# exit

eventFile = 0
channelFile = 0



with open(epgdatafile, 'r') as f:
    for line in f:
        if line[0] == 'C':
            parseChannel(line[2:])
        if line[0] == 'E':
            parseEvent(line[2:])
        if line[0] == 'T':
            parseTitle(line[2:])
        if line[0] == 'S':
            parseShortText(line[2:])
        if line[0] == 'D':
            parseDescription(line[2:])
        if line[0] == 'e':
            parseEventEnd()
#             break
        if line[0] == 'c':
            parseChannelEnd()
    
    
    